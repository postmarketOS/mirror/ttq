# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import re
import ttq.cache

REGEX_HTML_TAGS = re.compile('<.*?>')


def strip_html(html):
   return re.sub(REGEX_HTML_TAGS, " ", html).strip()


def parse_table_tr(html):
    tds = html.split("<td>")

    for i in range(1, len(tds)):
        tds[i] = strip_html(tds[i])

    return tds


def parse_list(html):
    lis = html.split("<li>")
    lis.pop(0)

    for i in range(0, len(lis)):
        lis[i] = lis[i].split("</li>", 1)[0]
        lis[i] = strip_html(lis[i])

    return lis


def read_from_cache(cache_name, start=None, end=None):
    html_path = f"{ttq.cache.CACHE_DIR}/{cache_name}"
    with open(html_path, "r") as h:
        ret = h.read()

    if start:
        assert start in ret, f"can't find start in {cache_name}: {start}"
        ret = ret.split(start, 1)[1]

    if end:
        assert end in ret, f"can't find end in {cache_name}: {end}"
        ret = ret.split(end, 1)[0]

    return ret


def parse_table(cache_name, start, end):
    html = read_from_cache(cache_name, start, end)
    ret = []

    trs = html.split("<tr>")
    for i in range(2, len(trs)):
        ret += [parse_table_tr(trs[i])]

    return ret
